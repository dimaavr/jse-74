package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.listener.AbstractListener;

import java.util.Collection;

public interface IListenerService {

    @Nullable
    AbstractListener getCommandByName(@Nullable String name);

    @Nullable
    AbstractListener getCommandByArg(@Nullable String arg);

    @NotNull
    Collection<AbstractListener> getCommands();

    @NotNull
    Collection<AbstractListener> getArguments();

    void add(@NotNull AbstractListener command);

}