package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.endpoint.Session;
import ru.tsc.avramenko.tm.endpoint.SessionDTO;

public interface ISessionService {

    @Nullable
    SessionDTO getSession();

    void setSession(@Nullable SessionDTO session);

}