package ru.tsc.avramenko.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.endpoint.AdminDataEndpoint;
import ru.tsc.avramenko.tm.endpoint.Role;
import ru.tsc.avramenko.tm.endpoint.SessionDTO;
import ru.tsc.avramenko.tm.event.ConsoleEvent;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import java.util.Optional;

@Component
public class DataBinSaveListener extends AbstractDataListener {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "data-bin-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save binary data.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinSaveListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        adminDataEndpoint.saveDataBin(session);
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}