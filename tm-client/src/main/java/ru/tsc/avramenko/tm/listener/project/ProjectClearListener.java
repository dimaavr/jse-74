package ru.tsc.avramenko.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.listener.AbstractProjectListener;
import ru.tsc.avramenko.tm.endpoint.ProjectEndpoint;
import ru.tsc.avramenko.tm.endpoint.Role;
import ru.tsc.avramenko.tm.endpoint.SessionDTO;
import ru.tsc.avramenko.tm.event.ConsoleEvent;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;

import java.util.Optional;

@Component
public class ProjectClearListener extends AbstractProjectListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Removing all projects.";
    }

    @Override
    @EventListener(condition = "@projectClearListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        projectEndpoint.clearProject(session);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}