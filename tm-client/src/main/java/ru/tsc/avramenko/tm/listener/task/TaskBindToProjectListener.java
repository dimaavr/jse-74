package ru.tsc.avramenko.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.listener.AbstractTaskListener;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.event.ConsoleEvent;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public class TaskBindToProjectListener extends AbstractTaskListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "task-bind-to-project";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Bind task to project.";
    }

    @Override
    @EventListener(condition = "@taskBindToProjectListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = projectEndpoint.findProjectById(session, projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final TaskDTO task = taskEndpoint.findTaskById(session, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final TaskDTO taskToProject = taskEndpoint.bindTaskById(session, projectId, taskId);
        if (taskToProject == null) throw new ProcessException();

    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}