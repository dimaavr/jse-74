package ru.tsc.avramenko.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.listener.AbstractUserListener;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.event.ConsoleEvent;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

@Component
public class UserChangeRoleListener extends AbstractUserListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "user-change-role";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change the user role.";
    }

    @Override
    @EventListener(condition = "@userChangeRoleListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER USER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final UserDTO user = adminUserEndpoint.findUserById(session, id);
        if (user == null) throw new UserNotFoundException();
        System.out.println("ENTER NEW ROLE: ");
        System.out.println(Arrays.toString(Role.values()));
        @Nullable final String roleValue = TerminalUtil.nextLine();
        @NotNull final Role role = Role.valueOf(roleValue);
        adminUserEndpoint.setUserRole(session, id, role);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}