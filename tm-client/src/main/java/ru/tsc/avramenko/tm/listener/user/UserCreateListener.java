package ru.tsc.avramenko.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.listener.AbstractUserListener;
import ru.tsc.avramenko.tm.endpoint.UserEndpoint;
import ru.tsc.avramenko.tm.event.ConsoleEvent;
import ru.tsc.avramenko.tm.util.TerminalUtil;

@Component
public class UserCreateListener extends AbstractUserListener {

    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String name() {
        return "user-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new user.";
    }

    @Override
    @EventListener(condition = "@userCreateListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL: ");
        @Nullable final String email = TerminalUtil.nextLine();
        userEndpoint.registryUser(login, password, email);
    }

}