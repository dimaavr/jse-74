package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.repository.IListenerRepository;
import ru.tsc.avramenko.tm.api.service.IListenerService;
import ru.tsc.avramenko.tm.listener.AbstractListener;

import java.util.Collection;

@Service
public class ListenerService implements IListenerService {

    @Autowired
    private IListenerRepository listenerRepository;

    @Nullable
    @Override
    public AbstractListener getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return listenerRepository.getCommandByName(name);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return listenerRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractListener> getCommands() {
        return listenerRepository.getCommands();
    }

    @Override
    public Collection<AbstractListener> getArguments() {
        return listenerRepository.getArguments();
    }

    @Override
    public void add(@Nullable AbstractListener command) {
        if (command == null) return;
        listenerRepository.add(command);
    }

}