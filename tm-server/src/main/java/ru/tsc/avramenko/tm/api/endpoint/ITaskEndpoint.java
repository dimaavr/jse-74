package ru.tsc.avramenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    TaskDTO bindTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @WebMethod
    void createTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    );

    @Nullable
    @WebMethod
    void clearTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

    @Nullable
    @WebMethod
    List<TaskDTO> findTaskAll(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @Nullable
    @WebMethod
    List<TaskDTO> findTaskByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    TaskDTO finishTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    TaskDTO finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    TaskDTO finishTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void removeTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    void removeTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    TaskDTO changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    TaskDTO changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    TaskDTO changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    TaskDTO startTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    TaskDTO startTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    TaskDTO startTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    TaskDTO unbindTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @WebMethod
    TaskDTO updateTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @WebMethod
    TaskDTO updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

}