package ru.tsc.avramenko.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.IBroadcastService;
import ru.tsc.avramenko.tm.service.BroadcastService;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

@Getter
@Component
public class JmsMessageComponent {

    @NotNull
    public static JmsMessageComponent instance;

    @NotNull
    final IBroadcastService broadcastService = new BroadcastService();

    @NotNull
    private Connection connection;

    @NotNull
    private ConnectionFactory connectionFactory;

    public JmsMessageComponent() {
        init();
        instance = this;
    }

    @NotNull
    public static JmsMessageComponent getInstance() {
        return instance;
    }

    @SneakyThrows
    private void init() {
        connectionFactory = new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL
        );
        connection = connectionFactory.createConnection();
    }

    public void run() throws JMSException {
        connection.start();
    }

    public void shutdown() throws JMSException {
        connection.close();
        broadcastService.shutdown();
    }

}