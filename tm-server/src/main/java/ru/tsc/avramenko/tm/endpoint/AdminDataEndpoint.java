package ru.tsc.avramenko.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.avramenko.tm.api.endpoint.IAdminDataEndpoint;
import ru.tsc.avramenko.tm.api.service.IDataService;
import ru.tsc.avramenko.tm.api.service.dto.ISessionDtoService;
import ru.tsc.avramenko.tm.component.Backup;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.InetAddress;

@WebService
@Controller
public class AdminDataEndpoint extends AbstractEndpoint implements IAdminDataEndpoint {

    @Autowired
    private Backup backup;

    @Autowired
    private ISessionDtoService sessionDtoService;

    @Autowired
    private IDataService dataService;

    @Override
    @Nullable
    @WebMethod
    public void loadBackup(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        backup.load();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveBackup(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        backup.run();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataBase64(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.loadDataBase64();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataBase64(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.saveDataBase64();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataBin(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.loadDataBin();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataBin(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.saveDataBin();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataJson(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.loadDataJson();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataJson(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.saveDataJson();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataXml(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.loadDataXml();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataXml(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.saveDataXml();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataYaml(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.loadDataYaml();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataYaml(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.saveDataYaml();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataJsonJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.loadDataJsonJaxB();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataJsonJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.saveDataJsonJaxB();
    }

    @Override
    @Nullable
    @WebMethod
    public void loadDataXmlJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.loadDataXmlJaxB();
    }

    @Override
    @Nullable
    @WebMethod
    public void saveDataXmlJaxB(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        dataService.saveDataXmlJaxB();
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public String getServerHost(
    ) {
        return InetAddress.getLocalHost().getHostName();
    }

}