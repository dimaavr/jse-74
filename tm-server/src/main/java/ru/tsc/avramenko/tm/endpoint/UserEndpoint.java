package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.avramenko.tm.api.endpoint.IUserEndpoint;
import ru.tsc.avramenko.tm.api.service.dto.ISessionDtoService;
import ru.tsc.avramenko.tm.api.service.dto.IUserDtoService;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Autowired
    private ISessionDtoService sessionDtoService;

    @Autowired
    private IUserDtoService userDtoService;

    @Override
    @Nullable
    @WebMethod
    public boolean existsUserById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionDtoService.validate(session);
        return userDtoService.isUserExist(id);
    }

    @Override
    @Nullable
    @WebMethod
    public boolean existsUserByEmail(
            @WebParam(name = "email", partName = "email") @NotNull final String email
    ) {
        return userDtoService.isEmailExist(email);
    }

    @Override
    @Nullable
    @WebMethod
    public boolean existsUserByLogin(
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        return userDtoService.isLoginExist(login);
    }

    @Override
    @Nullable
    @WebMethod
    public SessionDTO registryUser(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password,
            @WebParam(name = "email", partName = "email") final String email
    ) {
        userDtoService.create(login, password, email);
        return sessionDtoService.open(login, password);
    }

}