package ru.tsc.avramenko.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.service.model.ITaskService;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.*;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.repository.model.TaskRepository;
import ru.tsc.avramenko.tm.repository.model.UserRepository;
import ru.tsc.avramenko.tm.service.AbstractService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService extends AbstractService implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = userRepository.findUserById(userId);
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setName(name);
        taskRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final User user = userRepository.findUserById(userId);
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.findByIndex(userId).get(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.removeByName(userId, name);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = taskRepository.findByIndex(userId).get(index);
        taskRepository.delete(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findByIndex(userId).get(index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findByIndex(userId).get(index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findByIndex(userId).get(index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findByIndex(userId).get(index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        taskRepository.save(task);
        return task;
    }

    @Override
    public @Nullable Task findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(userId, id);
    }

    @Override
    @Transactional
    public @Nullable void removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.removeById(userId, id);
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllById(userId);
    }

    @Override
    public @Nullable List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        taskRepository.clear(userId);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.clear();
    }

    @Override
    @Transactional
    public void addAll(@Nullable List<Task> tasks) {
        if (tasks == null) return;
        taskRepository.saveAll(tasks);
    }

}