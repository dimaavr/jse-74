package ru.tsc.avramenko.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.service.ProjectService;
import ru.tsc.avramenko.tm.service.TaskService;
import ru.tsc.avramenko.tm.util.UserUtil;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @GetMapping("/task/create")
    public ModelAndView create() {
        final Task task = new Task();
        task.setUserId(UserUtil.getUserId());
        return new ModelAndView("task-create", "task", task);
    }

    @PostMapping("/task/create")
    public String create(
            @ModelAttribute("task") Task task, BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.create(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        @Nullable final String userId = UserUtil.getUserId();
        taskService.removeByIdAndUserId(id, userId);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        @Nullable final String userId = UserUtil.getUserId();
        final Task task = taskService.findByIdAndUserId(id, userId);
        return new ModelAndView("task-edit", "task", task);
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task, BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.save(task);
        return "redirect:/tasks";
    }

    @ModelAttribute("projects")
    public Collection<Project> getProjects() {
        @Nullable final String userId = UserUtil.getUserId();
        return projectService.findAllByCurrentUserId(userId);
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

}