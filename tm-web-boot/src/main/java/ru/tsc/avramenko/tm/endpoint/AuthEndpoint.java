package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.model.Result;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Optional;

@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.tsc.avramenko.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint implements IAuthEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Override
    @WebMethod
    @PostMapping("/login")
    public Result login(
            @WebParam(name = "username") @RequestParam("username") final String username,
            @WebParam(name = "password") @RequestParam("password") final String password
    ) {
        try {
            @Nullable final User user = userService.findByLogin(username);
            Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (Exception e) {
            return new Result(e);
        }
    }

    @Override
    @WebMethod
    @GetMapping("/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

    @Override
    @WebMethod
    @GetMapping("/profile")
    public Result profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @Nullable final User user = userService.findByLogin(authentication.getName());
        return new Result(user);
    }

}