package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.service.ProjectService;
import ru.tsc.avramenko.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.avramenko.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Project find(
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        @Nullable final String userId = UserUtil.getUserId();
        return projectService.findByIdAndUserId(id, userId);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        @Nullable final String userId = UserUtil.getUserId();
        return projectService.findAllByCurrentUserId(userId);
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Project create(
            @WebParam(name = "project")
            @RequestBody final Project project
    ) {
        project.setUserId(UserUtil.getUserId());
        return projectService.create(project);
    }

    @Override
    @WebMethod
    @PostMapping("/createAll")
    public List<Project> createAll(
            @WebParam(name = "projects")
            @RequestBody final List<Project> projects
    ) {
        projects.forEach(project -> project.setUserId(UserUtil.getUserId()));
        projects.forEach(projectService::create);
        return projects;
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Project save(
            @WebParam(name = "project")
            @RequestBody final Project project
    ) {
        project.setUserId(UserUtil.getUserId());
        return projectService.save(project);
    }

    @Override
    @WebMethod
    @PostMapping("/saveAll")
    public List<Project> saveAll(
            @WebParam(name = "projects")
            @RequestBody final List<Project> projects
    ) {
        projects.forEach(project -> project.setUserId(UserUtil.getUserId()));
        projects.forEach(projectService::save);
        return projects;
    }

    @Override
    @WebMethod
    @PostMapping("/delete/{id}")
    public void delete(
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        @Nullable final String userId = UserUtil.getUserId();
        projectService.removeByIdAndUserId(id, userId);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll() {
        @Nullable final String userId = UserUtil.getUserId();
        projectService.clearAllByCurrentUserId(userId);
    }

}