package ru.tsc.avramenko.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class AbstractOwnerRecord extends AbstractRecord {

    @Column(name = "user_id")
    protected String userId;

}