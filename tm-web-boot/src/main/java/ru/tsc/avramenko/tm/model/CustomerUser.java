package ru.tsc.avramenko.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Getter
@Setter
public class CustomerUser extends User {

    @Nullable
    private String userId = null;

    public CustomerUser(
            @NotNull final String username, @NotNull final String password,
            @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(username, password, authorities);
    }

    public CustomerUser(
            @NotNull final String username, @NotNull final String password,
            final boolean enabled, final boolean accountNonExpired,
            final boolean credentialsNonExpired, final boolean accountNonLocked,
            @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(
                username, password, enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, authorities
        );
    }

    public CustomerUser(@NotNull final UserDetails user) {
        super(
                user.getUsername(), user.getPassword(), user.isEnabled(),
                user.isAccountNonExpired(), user.isCredentialsNonExpired(),
                user.isAccountNonLocked(), user.getAuthorities()
        );
    }

    public CustomerUser withUserId(@NotNull final String userId) {
        this.userId = userId;
        return this;
    }

}