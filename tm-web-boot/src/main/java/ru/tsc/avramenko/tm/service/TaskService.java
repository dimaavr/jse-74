package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.service.ITaskService;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.repository.TaskRepository;

import java.util.Collection;

@Service
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @Override
    public @Nullable Task findById(@Nullable String id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public @Nullable Task findByIdAndUserId(@Nullable String id, @Nullable String userId) {
        return repository.findByIdAndUserId(id, userId);
    }

    @Override
    public @Nullable Collection<Task> findAll() {
        return repository.findAll();
    }

    @Override
    public @Nullable Collection<Task> findAllByCurrentUserId(@Nullable String userId) {
        return repository.findAllByCurrentUserId(userId);
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void clearAllByCurrentUserId(@Nullable String userId) {
        repository.removeAllByCurrentUserId(userId);
    }

    @Override
    @Transactional
    public @NotNull Task create(@Nullable Task task) {
        return repository.save(task);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByIdAndUserId(@Nullable String id, @Nullable String userId) {
        repository.removeByIdAndUserId(id, userId);
    }

    @Override
    @Transactional
    public @NotNull Task save(@NotNull Task task) {
        return repository.save(task);
    }

}