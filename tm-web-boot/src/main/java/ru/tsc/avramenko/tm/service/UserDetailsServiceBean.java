package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.service.IUserService;
import ru.tsc.avramenko.tm.model.CustomerUser;
import ru.tsc.avramenko.tm.model.Role;
import ru.tsc.avramenko.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    @NotNull
    private IUserService userService;

    @Nullable
    public User findByLogin(@Nullable final String login) {
        return userService.findByLogin(login);
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@NotNull final String username)
            throws UsernameNotFoundException {
        @Nullable final User user = findByLogin(username);
        Optional.ofNullable(user).orElseThrow(() ->
                new UsernameNotFoundException("User not found.")
        );
        @NotNull final List<Role> userRoles = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        for (Role role : userRoles) roles.add(role.toString());
        return new CustomerUser(
                org.springframework.security.core.userdetails.User
                        .withUsername(username)
                        .password(user.getPasswordHash())
                        .roles(roles.toArray(new String[]{}))
                        .build()
        ).withUserId(user.getId());
    }

}