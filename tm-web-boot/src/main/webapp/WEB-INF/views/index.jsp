<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:include page="../include/_header.jsp"/>
<sec:authorize access="isAuthenticated()">
    <h1>Welcome To Task Manager!
    You can open your projects or tasks.
    And also create, edit and delete them.
    Try it!</h1>
</sec:authorize>
<sec:authorize access="!isAuthenticated()">
    <h1>Welcome to Task Manager! Please login before you start!</h1>
</sec:authorize>
<jsp:include page="../include/_footer.jsp"/>