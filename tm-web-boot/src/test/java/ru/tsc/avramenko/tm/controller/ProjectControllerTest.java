package ru.tsc.avramenko.tm.controller;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.avramenko.tm.api.service.IProjectService;
import ru.tsc.avramenko.tm.api.service.IUserService;
import ru.tsc.avramenko.tm.marker.WebCategory;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.util.UserUtil;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@SpringBootTest
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectControllerTest {

    @Autowired
    @NotNull
    private IProjectService projectService;

    @Autowired
    @NotNull
    private IUserService userService;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    private static boolean initialized = false;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    @NotNull
    private WebApplicationContext wac;

    @NotNull
    private final Project project1 = new Project("Project1");

    @NotNull
    private final Project project2 = new Project("Project2");

    @Before
    public void initializeDB() {
        if (!initialized) {
            userService.create("projectController", "projectController");
            initialized = true;
        }
    }

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("projectController", "projectController");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        projectService.create(project1);
        projectService.create(project2);
    }

    @Test
    @SneakyThrows
    @Category(WebCategory.class)
    public void findAll() {
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project-list"));
    }

    @Test
    @SneakyThrows
    @Category(WebCategory.class)
    public void create() {
        @NotNull final String url = "/project/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project-create"));
    }

    @Test
    @SneakyThrows
    @Category(WebCategory.class)
    public void delete() {
        @NotNull final String url = "/project/delete/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects"));
        Assert.assertNull(projectService.findByIdAndUserId(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @SneakyThrows
    @Category(WebCategory.class)
    public void edit() {
        @NotNull final String url = "/project/edit/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("project-edit"));
    }

}
