package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.model.Project;

import java.util.Collection;

public interface IProjectService {

    @Nullable Project findById(@Nullable String id);

    @Nullable Project findByIdAndUserId(@Nullable String id, @Nullable String userId);

    @Nullable Collection<Project> findAll();

    @Nullable Collection<Project> findAllByCurrentUserId(@Nullable String userId);

    void clear();

    void clearAllByCurrentUserId(@Nullable String userId);

    @NotNull Project create(@Nullable Project project);

    void removeById(@Nullable String id);

    void removeByIdAndUserId(@Nullable String id, @Nullable String userId);

    @NotNull Project save(@NotNull Project project);

}