package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;

import java.util.Collection;

public interface ITaskService {

    @Nullable Task findById(@Nullable String id);

    @Nullable Task findByIdAndUserId(@Nullable String id, @Nullable String userId);

    @Nullable Collection<Task> findAll();

    @Nullable Collection<Task> findAllByCurrentUserId(@Nullable String userId);

    void clear();

    void clearAllByCurrentUserId(@Nullable String userId);

    @NotNull Task create(@Nullable Task task);

    void removeById(@Nullable String id);

    void removeByIdAndUserId(@Nullable String id, @Nullable String userId);

    @NotNull Task save(@NotNull Task task);

}