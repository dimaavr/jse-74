package ru.tsc.avramenko.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.tsc.avramenko.tm.model.Task;
import java.util.Collection;
import java.util.List;

public interface TaskEndpointClient {

    static TaskEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter =
                new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters =
                new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory =
                () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskEndpointClient.class, "http://localhost:8081/api/tasks");
    }

    @GetMapping("/find/{id}")
    Task find(@PathVariable("id") String id);

    @GetMapping("/findAll")
    Collection<Task> findAll();

    @PostMapping("/create")
    Task create(@RequestBody Task task);

    @PostMapping("/createAll")
    List<Task> createAll(@RequestBody List<Task> tasks);

    @PostMapping("/save")
    Task save(@RequestBody Task task);

    @PostMapping("/saveAll")
    List<Task> saveAll(@RequestBody List<Task> tasks);

    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll();

}