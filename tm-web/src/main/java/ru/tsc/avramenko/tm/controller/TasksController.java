package ru.tsc.avramenko.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.avramenko.tm.service.TaskService;
import ru.tsc.avramenko.tm.util.UserUtil;

@Controller
public class TasksController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/tasks")
    public ModelAndView index() {
        @Nullable final String userId = UserUtil.getUserId();
        return new ModelAndView("task-list", "tasks", taskService.findAllByCurrentUserId(userId));
    }

}