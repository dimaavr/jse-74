package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.service.TaskService;
import ru.tsc.avramenko.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.tsc.avramenko.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Task find(
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        @Nullable final String userId = UserUtil.getUserId();
        return taskService.findByIdAndUserId(id, userId);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        @Nullable final String userId = UserUtil.getUserId();
        return taskService.findAllByCurrentUserId(userId);
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Task create(
            @WebParam(name = "task")
            @RequestBody final Task task
    ) {
        task.setUserId(UserUtil.getUserId());
        return taskService.create(task);
    }

    @Override
    @WebMethod
    @PostMapping("/createAll")
    public List<Task> createAll(
            @WebParam(name = "tasks")
            @RequestBody final List<Task> tasks
    ) {
        tasks.forEach(task -> task.setUserId(UserUtil.getUserId()));
        tasks.forEach(taskService::create);
        return tasks;
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Task save(
            @WebParam(name = "task")
            @RequestBody final Task task
    ) {
        task.setUserId(UserUtil.getUserId());
        return taskService.save(task);
    }

    @Override
    @WebMethod
    @PostMapping("/saveAll")
    public List<Task> saveAll(
            @WebParam(name = "tasks")
            @RequestBody final List<Task> tasks
    ) {
        tasks.forEach(task -> task.setUserId(UserUtil.getUserId()));
        tasks.forEach(taskService::save);
        return tasks;
    }

    @Override
    @WebMethod
    @PostMapping("/delete/{id}")
    public void delete(
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        @Nullable final String userId = UserUtil.getUserId();
        taskService.removeByIdAndUserId(id, userId);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll() {
        @Nullable final String userId = UserUtil.getUserId();
        taskService.clearAllByCurrentUserId(userId);
    }

}