package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.configuration.DataBaseConfiguration;
import ru.tsc.avramenko.tm.marker.WebCategory;
import ru.tsc.avramenko.tm.model.Task;

import java.util.Collection;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class TaskRepositoryTest {

    @NotNull
    private static final String USER_ID = "123-456-789";

    @Autowired
    private TaskRepository taskRepository;

    @Before
    public void before() {
        taskRepository.removeAllByCurrentUserId(USER_ID);
    }

    @Test
    @Category(WebCategory.class)
    public void findAllByCurrentUserId() {
        @NotNull final Task task = new Task("task");
        task.setUserId(USER_ID);
        taskRepository.save(task);
        @Nullable final Collection<Task> taskNew = taskRepository.findAllByCurrentUserId(USER_ID);
        Assert.assertEquals(1, taskNew.size());
    }

    @Test
    @Category(WebCategory.class)
    public void findByIdAndUserId() {
        @NotNull final Task task = new Task("task");
        task.setUserId(USER_ID);
        taskRepository.save(task);
        @Nullable final Task taskNew = taskRepository.findByIdAndUserId(task.getId(), USER_ID);
        Assert.assertNotNull(taskNew);
    }

    @Test
    @Category(WebCategory.class)
    public void removeByIdAndUserId() {
        @NotNull final Task task = new Task("task");
        task.setUserId(USER_ID);
        taskRepository.save(task);
        taskRepository.removeByIdAndUserId(task.getId(), USER_ID);
        Assert.assertEquals(0, taskRepository.findAllByCurrentUserId(USER_ID).size());
    }

    @Test
    @Category(WebCategory.class)
    public void removeAllByCurrentUserId() {
        @NotNull final Task task = new Task("task");
        @NotNull final Task task2 = new Task("task2");
        task.setUserId(USER_ID);
        task2.setUserId(USER_ID);
        taskRepository.save(task);
        taskRepository.save(task2);
        final Integer size = taskRepository.findAllByCurrentUserId(USER_ID).size();
        taskRepository.removeAllByCurrentUserId(USER_ID);
        final Integer size1 = taskRepository.findAllByCurrentUserId(USER_ID).size();
        Assert.assertNotEquals(size, size1);
    }

}