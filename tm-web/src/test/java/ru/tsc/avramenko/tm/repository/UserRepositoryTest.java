package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.configuration.DataBaseConfiguration;
import ru.tsc.avramenko.tm.marker.WebCategory;
import ru.tsc.avramenko.tm.model.User;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class UserRepositoryTest {

    @NotNull
    private static String USER_ID;

    @Autowired
    private UserRepository userRepository;

    private static boolean initialized = false;

    private static User user = new User();
    private static User userTwo = new User();

    @Before
    public void initializeDB() {
        if (!initialized) {
            user.setLogin("user1");
            user.setPasswordHash("user1");
            user.setEmail("user1@email.ru");
            userRepository.save(user);
            userTwo.setLogin("userTwo");
            userTwo.setPasswordHash("userTwo");
            userTwo.setEmail("userTwo@email.ru");
            userRepository.save(userTwo);
            USER_ID = userRepository.findByLogin("user1").getId();
            initialized = true;
        }
    }

    @Test
    @Category(WebCategory.class)
    public void findUserById() {
        final User user = userRepository.findUserById(USER_ID);
        Assert.assertNotNull(user);
    }

    @Test
    @Category(WebCategory.class)
    public void findByLogin() {
        final User userFind = userRepository.findByLogin(userTwo.getLogin());
        Assert.assertNull(userFind);
    }

    @Test
    @Category(WebCategory.class)
    public void findByEmail() {
        final User userFind = userRepository.findByEmail(userTwo.getEmail());
        Assert.assertNull(userFind);
    }

    @Test
    @Category(WebCategory.class)
    public void removeUserByLogin() {
        userRepository.removeUserByLogin("userTwo");
        final User user = userRepository.findByLogin("userTwo");
        Assert.assertNull(user);
    }

}