package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.avramenko.tm.api.service.IProjectService;
import ru.tsc.avramenko.tm.api.service.IUserService;
import ru.tsc.avramenko.tm.configuration.DataBaseConfiguration;
import ru.tsc.avramenko.tm.marker.WebCategory;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.util.UserUtil;

import java.util.Collection;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class ProjectServiceTest {

    @NotNull
    private static String USER_ID;

    private static boolean initialized = false;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @Before
    public void initializeDB() {
        if (!initialized) {
            userService.create("projectService", "projectService");
            initialized = true;
        }
    }

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("projectService", "projectService");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
    }

    @Test
    @Category(WebCategory.class)
    public void create() {
        @NotNull final Project project = new Project("project");
        project.setUserId(USER_ID);
        projectService.create(project);
        @Nullable final Project projectNew = projectService.findByIdAndUserId(project.getId(), USER_ID);
        Assert.assertNotNull(projectNew);
    }

    @Test
    @Category(WebCategory.class)
    public void clear() {
        projectService.clearAllByCurrentUserId(USER_ID);
        @Nullable final Collection<Project> projectNew = projectService.findAllByCurrentUserId(USER_ID);
        Assert.assertEquals(0, projectNew.size());
    }

    @Test
    @Category(WebCategory.class)
    public void deleteById() {
        @NotNull final Project project = new Project("project");
        project.setUserId(USER_ID);
        projectService.create(project);
        projectService.removeByIdAndUserId(project.getId(), USER_ID);
        @Nullable final Project projectFind = projectService.findByIdAndUserId(project.getId(), USER_ID);
        Assert.assertNull(projectFind);
    }


    @Test
    @Category(WebCategory.class)
    public void findById() {
        @NotNull final Project project = new Project("project");
        project.setUserId(USER_ID);
        projectService.create(project);
        @Nullable final Project projectFind = projectService.findByIdAndUserId(project.getId(), USER_ID);
        Assert.assertNotNull(projectFind);
    }

    @Test
    @Category(WebCategory.class)
    public void findAll() {
        @NotNull final Project project = new Project("project2");
        project.setUserId(USER_ID);
        projectService.create(project);
        @Nullable final Collection<Project> projectNew = projectService.findAllByCurrentUserId(USER_ID);
        Assert.assertNotEquals(0, projectNew.size());
    }

}