package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.avramenko.tm.api.service.ITaskService;
import ru.tsc.avramenko.tm.api.service.IUserService;
import ru.tsc.avramenko.tm.configuration.DataBaseConfiguration;
import ru.tsc.avramenko.tm.marker.WebCategory;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.util.UserUtil;

import java.util.Collection;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class TaskServiceTest {

    @NotNull
    private static String USER_ID;

    private static boolean initialized = false;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IUserService userService;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @Before
    public void initializeDB() {
        if (!initialized) {
            userService.create("taskService", "taskService");
            initialized = true;
        }
    }

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("taskService", "taskService");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
    }

    @Test
    @Category(WebCategory.class)
    public void create() {
        @NotNull final Task task = new Task("task");
        task.setUserId(USER_ID);
        taskService.create(task);
        @Nullable final Task taskNew = taskService.findByIdAndUserId(task.getId(), USER_ID);
        Assert.assertNotNull(taskNew);
    }

    @Test
    @Category(WebCategory.class)
    public void clear() {
        taskService.clearAllByCurrentUserId(USER_ID);
        @Nullable final Collection<Task> taskNew = taskService.findAllByCurrentUserId(USER_ID);
        Assert.assertEquals(0, taskNew.size());
    }

    @Test
    @Category(WebCategory.class)
    public void deleteById() {
        @NotNull final Task task = new Task("task");
        task.setUserId(USER_ID);
        taskService.create(task);
        taskService.removeByIdAndUserId(task.getId(), USER_ID);
        @Nullable final Task taskFind = taskService.findByIdAndUserId(task.getId(), USER_ID);
        Assert.assertNull(taskFind);
    }


    @Test
    @Category(WebCategory.class)
    public void findById() {
        @NotNull final Task task = new Task("task");
        task.setUserId(USER_ID);
        taskService.create(task);
        @Nullable final Task taskFind = taskService.findByIdAndUserId(task.getId(), USER_ID);
        Assert.assertNotNull(taskFind);
    }

    @Test
    @Category(WebCategory.class)
    public void findAll() {
        @NotNull final Task task = new Task("task2");
        task.setUserId(USER_ID);
        taskService.create(task);
        @Nullable final Collection<Task> projectNew = taskService.findAllByCurrentUserId(USER_ID);
        Assert.assertNotEquals(0, projectNew.size());
    }

}